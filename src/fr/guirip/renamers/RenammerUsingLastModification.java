package fr.guirip.renamers;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * THIS BATCH:
 *  - RENAMES FILES USING THEIR LAST MODIFICATION DATE `file.lastModified()`
 *  - DOES NOT CREATE ONE SUB-DIRECTORY PER DAY
 * @author guillaume
 */
public class RenammerUsingLastModification {

	public static final boolean SIMULATION = false;
	
	public static final String PATH = "/media/guillaume/Guigui/Photos/Hike/2019_04_19-21 Foret dOrient/04_avril2019_Foret_d'Orient";
	public static final String DEST = "/media/guillaume/Guigui/Photos/Hike/2019_04_19-21 Foret dOrient/04_avril2019_Foret_d'Orient/renamed";

	public static final String SIM_SENTENCE = "Would move ";
	public static final String MOV_SENTENCE = "Moving ";
	
	private static DatedDestDirsHandler datedDirsHandler = new DatedDestDirsHandler(DEST);
	
	public static final SimpleDateFormat dFormat = new SimpleDateFormat("yyyy_MM_dd");
	public static final SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy_MM_dd_HH'h'mm'm'");

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			File fParent = new File(PATH);
			if (!fParent.exists()){
				throw new FileNotFoundException(PATH);
			}
			File[] fImages = fParent.listFiles();

			// Parcours
			for (File fChild : fImages){
				if (fChild.isDirectory()){
					System.out.println("Ignored directory " + fChild.getName());
				} else {
					// Date directory
					File destDir = datedDirsHandler.getDatedDir(dFormat.format(new Date(fChild.lastModified())));
					
					// File destination
					File destFile = new File(destDir, dtFormat.format(fChild.lastModified()) + "_" + fChild.getName());
					
					// Move file
					if (!SIMULATION){
						fChild.renameTo(destFile);
					}
					System.out.println((SIMULATION ? SIM_SENTENCE : MOV_SENTENCE) + fChild.getName() + " to " + destFile.getPath());
				}
			}
		
		} catch (Exception e){
			e.printStackTrace();
		}
	}

}
