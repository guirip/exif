package fr.guirip.renamers;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * THIS BATCH:
 *  - APPLIES A REGEXP ON FILE NAMES TO RECONSTRUCT THE APPROPRIATE DATE PREFIX
 *  - DOES NOT CREATE ONE SUB-DIRECTORY PER DAY
 * 
 * @author guillaume
 */
public class RenammerUsingRegExp {

	public static final boolean SIMULATION = false;
	
	public static final String PATH = "/perso/_photos/phone-new";
	public static final String DEST = "/perso/_photos/phone-new-renamed";

	public static final String SIM_SENTENCE = "Would move ";
	public static final String MOV_SENTENCE = "Moving ";
	
	private static DatedDestDirsHandler datedDirsHandler = new DatedDestDirsHandler(DEST);

	private static final Pattern p = Pattern.compile(
//			"\\d{4}_\\d{2}_\\d{2}_\\d{2}h\\d{2}m_IMG_(\\d{4})(\\d{2})(\\d{2})_(\\d{2})(\\d{2})\\d{2}\\.jpg)");
			"[TRVIDMG]{3,4}_(\\d{4})(\\d{2})(\\d{2})_(\\d{2})(\\d{2})\\d{2}(\\d{3})?.*\\.[mp4jJpP3gG]{3}");
	
	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		redirect(new File(PATH));
	}
	
	/**
	 * 
	 * @param file
	 */
	private static void redirect(File file) {
		if (file.isDirectory()){
			handleDirectory(file);
		} else {
			handleFile(file);
		}
	}

	/**
	 * 
	 * @param child
	 */
	private static void handleDirectory(File dir) {
		System.out.println("Handling directory " + dir);
		
		File[] childs = dir.listFiles();
		for (File child : childs){
			redirect(child);
		}
	}

	/**
	 * 
	 * @param img
	 */
	private static void handleFile(File img) {
		Matcher m = p.matcher(img.getName());
		if (m.matches()){
			
			/*
			System.out.println("Matched :\n");
			System.out.println("0 : " + m.group(0) + "\n"); // Full string
			System.out.println("1 : " + m.group(1) + "\n"); // image name
			System.out.println("2 : " + m.group(2) + "\n"); // year
			System.out.println("3 : " + m.group(3) + "\n"); // month
			System.out.println("4 : " + m.group(4) + "\n"); // day
			System.out.println("5 : " + m.group(5) + "\n"); // hour
			System.out.println("6 : " + m.group(6) + "\n"); // minutes
			System.out.println("---------------------------\n");
			*/
			
			String date = m.group(1) + "_" + m.group(2) + "_" + m.group(3);
			File destFile = new File(
					datedDirsHandler.getDatedDir(date),
					date + "_" + m.group(4) + "h" + m.group(5) + "m_" + img.getName());

			if (!SIMULATION){
				boolean result = img.renameTo(destFile);
				if (!result) {
					System.out.println("Failed!");
				}
			}
			System.out.println((SIMULATION ? SIM_SENTENCE : MOV_SENTENCE) + img.getName() + " to " + destFile.getPath());
			
		} else {
			System.err.println("Cannot match " + img.getName());
		}
	}
	
}
