package fr.guirip.renamers;

import java.io.File;
import java.util.HashMap;

public class DatedDestDirsHandler extends HashMap<String, File> {

	private static final long serialVersionUID = 2768739050421123845L;

	private String DEST; 
	private File fDest;
//	private File[] destDirs;
	
	/**
	 * Constructor
	 * @param destination
	 */
	public DatedDestDirsHandler(String destination){
		super();
		
		DEST = destination;
		fDest = new File(DEST);
		if (fDest.exists() != true) {
			System.out.println("Auto creating directory: "+destination);
			fDest.mkdirs();
		}
//		destDirs = fDest.listFiles();
	}
	
	/**
	 * 
	 * @param sDate
	 * @return
	 */
	public File getDatedDir(String sDate){
		
		return fDest;
		/*
		File f = this.get(sDate);
		if (f == null){
			for (File destDir : destDirs){
				if (destDir.getName().startsWith(sDate)){
					f = destDir;
					break;
				}
			}
			if (f == null){
				f = new File(DEST, sDate);
				f.mkdirs();
			}
			this.put(sDate, f);
		}
		return f;*/
	}
}
