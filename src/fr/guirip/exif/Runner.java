package fr.guirip.exif;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.function.Consumer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import com.drew.metadata.mp4.Mp4Directory;

/**
 * 2/10/2019
 * 
 * This reads creation date from metadata to rename JPG / RW2 / MP4 files (case insensitive)
 * 
 * e.g
 *  rename/copy P1060408.JPG to 2019_07_24_09h12m06_P1060408.JPG
 *  rename/copy P1050625_eglise.RW2 to 2019_07_11_12h01m54_P1050625_eglise.RW2
 *  rename/copy P1050693.MP4 to 2019_07_13_09h22m40_P1050693.MP4
 * 
 * @author guillaume
 */
public class Runner {

	private static final boolean SIMULATION = false;
	private static final boolean PERFORM_RSYNC = false;
	private static final boolean COPY_INSTEAD_OF_RENAME = true;
	private static final boolean LOG_EXIF = false;
	
	public static final String PARAM_INPUT = "input";
	public static final String PARAM_ID = "id";
	
	
	// Smartphone
	/*
	public static final int DECALAGE_HORAIRE_PHOTO = -1;
	public static final int DECALAGE_HORAIRE_VIDEO = 0;

	private static final String[] inputs = { "/perso/_photos/phone-new" };
	
	public static final String DEST = "/perso/_photos/phone-new-renamed";
	*/
	// RX10 iii
	
	public static final int DECALAGE_HORAIRE_PHOTO = -1;
	public static final int DECALAGE_HORAIRE_VIDEO = 0;

	private static final String[] inputs = {
			"/media/guillaume/LUMIX/DCIM", // LX100 - photos
			"/media/guillaume/LUMIX/PRIVATE/AVCHD/BDMV/STREAM", // LX100 - videos
			"/media/guillaume/disk/DCIM/100MSDCF", // RX10 iii - photos
			"/media/guillaume/disk/MP_ROOT/100ANV01", // RX10 iii - video
			"/media/guillaume/disk/PRIVATE/M4ROOT/CLIP" }; // RX10 iii - ralentis (hfr)
		
	//public static final String DEST = "/media/guillaume/Samsung_T5/photos-rx10iii";
	// public static final String DEST = "/perso/_photos";
	public static final String DEST = "/media/guillaume/Samsung_T5/VIDEOS-GRAFFITI/2020_04_peintures_projet_saato";

	
	// private static final String input = System.getProperty(PARAM_INPUT);
	// private static final String id = System.getProperty(PARAM_ID);
	
	// private static final String input = "/media/guillaume/LUMIX/DCIM";
	// private static final String input = "/perso/_photos/PHONE"; //System.getProperty(PARAM_INPUT);
	
	private static final String id = System.getProperty(PARAM_ID);
//	private static final SimpleDateFormat destDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH'h'mm'm'ss");
	private static final SimpleDateFormat destDateFormat = new SimpleDateFormat("yyyy_MM_dd_HH'h'mm'm'");

//	private static final Pattern p = Pattern.compile("[\\d]{4}_[\\d]{2}_[\\d]{2}_[\\d]{2}h[\\d]{2}m[\\d]{2}(_.*)$");
	private static final Pattern p = Pattern.compile("\\d{4}_\\d{2}_\\d{2}_\\d{2}h\\d{2}m(_.*\\.[mMpP4jJpP3gGRW2]{3})");

	/**
	 * main method
	 * @param args
	 */
	public static void main(String[] args){
		for (int i=0; i<inputs.length; i++) {
			processInput(inputs[i]);
		}
		System.out.println("End.");
	}
	
	private static void processInput(String input) {
		if (input == null || input.equals("")){
			throw new IllegalArgumentException("Parameter " + PARAM_INPUT + " missing !");
		}

		File fInput = new File(input);
		if (!fInput.exists()){
			System.out.println(input + " doesnt exist");
		}
		else if (!fInput.canRead()){
			throw new IllegalArgumentException(input + " is not readable !");
		}
		else {
			System.out.println("Processing "+input);
			perform(fInput);
		}
		System.out.println("");
	}

	/**
	 * 
	 * @param fInput
	 */
	private static void perform(File fInput){
		if (fInput.isFile()){
			Date d = null;
			String fileName = fInput.getName();
			boolean isPhoto = false, isVideo = false;
			
			if (fileName.endsWith(".thm") || fileName.endsWith(".THM")
					|| fileName.endsWith(".xml") || fileName.endsWith(".XML")) {
//				System.out.println("Ignoring " + fInput.getPath());
			}
			else if (fileName.endsWith(".jpg") || fileName.endsWith(".JPG")
					|| fileName.endsWith(".rw2") || fileName.endsWith(".RW2")
					|| fileName.endsWith(".arw") || fileName.endsWith(".ARW")) {
				isPhoto = true;
			}
			else if (fileName.endsWith(".mp4") || fileName.endsWith(".MP4")
					|| fileName.endsWith(".mts") || fileName.endsWith(".MTS")){ // 3gp is not supported by the lib metadata-extractor :/
				isVideo = true;
			}
			else {
				System.out.println("Unmanaged file extension for file : " + fInput.getPath());
			}
			if (isPhoto || isVideo) {
				if (isPhoto) {
					d = getDateFromImage(fInput);
				} else if (isVideo) {
					d = getDateFromVideo(fInput);
				}
				if (d == null) {
					System.err.println("No capture date found from file : " + fInput.getPath());
				} else {
					performOnFile(fInput, d);
				}
			}
		} else {
			performOnDirectory(fInput);
		}
	}

	/**
	 * 
	 * @param fDirectory
	 */
	private static void performOnDirectory(File fDirectory) {
		File[] tfChildrens = fDirectory.listFiles();
		for (int i=0, total = tfChildrens.length; i<total; i++){
			perform(tfChildrens[i]);
		}
	}

	/**
	 * 
	 * @param fInput
	 * @return
	 */
	private static Metadata getMetaData(File fInput) {
		Metadata metadata = null;

		// Read metadata
		try {
			metadata = ImageMetadataReader.readMetadata(fInput);
			if (metadata == null){
				System.err.println("No metadata found from file " + fInput.getPath());
			}
		} catch (Exception e){
			if (e.getMessage().equals("File format could not be determined")) {
				//System.out.println("File not supported by exif library: "+ fInput.getName());
			} else {
				System.err.println("Error reading metadata from file "+fInput.getPath()+"  #  "+e.getClass().getName()+": "+e.getMessage());
			}
		}
		return metadata;
	}

	/**
	 * 
	 * @param fInput
	 * @return
	 */
	private static Date getDateFromVideo(File fInput) {
		Date d = null;

		Metadata metadata = getMetaData(fInput);
		if (metadata == null) {			
			// System.out.println("Fallback: last modification date "+d);
			d = new Date(fInput.lastModified());
		} else {

			Directory dir = metadata.getFirstDirectoryOfType(Mp4Directory.class);
			d = dir.getDate(Mp4Directory.TAG_CREATION_TIME);

			d.setTime(d.getTime()+(DECALAGE_HORAIRE_VIDEO*1000*60*60));

//			for (Directory directory : metadata.getDirectories()) {
//				for (Tag tag : directory.getTags()) {
//					System.out.format("[%s] - %s = %s\n", directory.getName(), tag.getTagName(), tag.getDescription());
//				}
//				if (directory.hasErrors()) {
//					for (String error : directory.getErrors()) {
//						System.err.format("ERROR: %s", error);
//					}
//				}
//			}
		}
		return d;
	}
	
	/**
	 * 
	 * @param fInput
	 * @return
	 */
	private static Date getDateFromImage(File fInput) {
		Metadata metadata = getMetaData(fInput);
		if (metadata != null) {
			if (LOG_EXIF) {
				metadata.getDirectories().forEach(new Consumer<Directory>() {
					  
					@Override
					public void accept(Directory d) { 
						System.out.println(d.toString());
						
						Collection<Tag> tags = d.getTags();
						tags.forEach(new Consumer<Tag>() {
	
							@Override
							public void accept(Tag t) { 
								// t.getTagName()+" "+
								System.out.println(t.toString());	
							}
						}); 
						System.out.println(" ");
					} 	
				});
			}
			
			Directory dir = metadata.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
			Date date = dir.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
			if (date == null) {
				dir = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
				date = dir.getDate(ExifIFD0Directory.TAG_DATETIME);
			}
			if (date != null) {
				date.setTime(date.getTime()+(DECALAGE_HORAIRE_PHOTO*1000*60*60));
				return date;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param fImage
	 * @param date
	 */
	private static void performOnFile(File fImage, Date date) {
		String newDateString = destDateFormat.format(date);

		if (fImage.getName().indexOf(newDateString) != -1) {
			// Unnecessary update
			System.out.println("Skipping "+fImage.getPath());
		} else {
			Matcher m = p.matcher(fImage.getName());
			String fileName;
			if (m.matches()) {
				fileName = m.group(1);
			} else {
				fileName = fImage.getName();
			}
				
			// File destFile = new File(fImage.getParentFile(), newName));
			File destFile = new File(DEST, newDateString + (id != null ? "_"+id : "") + "_" + fileName);
			

			if (PERFORM_RSYNC) {
				// rsync -tvn "$@" $destination
				String command = "rsync -tvv \""+fImage.getPath()+"\" \""+destFile.getPath()+"\"";
				System.out.println(command);
				String s;
				Process p;
				try {
					p = Runtime.getRuntime().exec(command);
					BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));
					
					while ((s = br.readLine()) != null) {
						System.out.println(s);
					}
					p.waitFor();
					//System.out.println ("exit: " + p.exitValue());
					p.destroy();
				} catch (Exception e) {
					System.err.println("Failed to execute command");
					e.printStackTrace();
				}
				
				//System.exit(0);
				
			} else if (destFile.exists()) {
				System.out.println(" > skipping - destination already exists: "+destFile.getPath());

			} else if (SIMULATION) {
				System.out.println("would "+(COPY_INSTEAD_OF_RENAME ? "copy " : "rename ")+fImage.getName()+" to "+destFile.getPath());

			} else if (COPY_INSTEAD_OF_RENAME) {
				// Copy
				try {
					FileUtils.copyFile(fImage, destFile);
					System.out.println("Successfully copied "+destFile.getPath());
				} catch (IOException e) {
					System.err.println("Failed to copy  "+fImage.getPath()+"  to  "+destFile.getPath());
					e.printStackTrace();
				}
				
			} else {
				// Rename
				fImage.renameTo(destFile);
				System.out.println("Renamed "+fImage.getPath()+"  to  "+destFile.getPath());
			}
		}
	}

}
